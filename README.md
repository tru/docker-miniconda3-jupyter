## installing a jupyter minimal environment in docker

CI produced registry image at registry-gitlab.pasteur.fr/tru/docker-miniconda3-jupyter

## singularity conversion if you have singularityCE or apptainer installed
```
singularity build docker-miniconda3-jupyter.sif  docker://registry-gitlab.pasteur.fr/tru/docker-miniconda3-jupyter:main
singularity exec docker-miniconda3-jupyter.sif jupyter-lab  --no-browser
```

## on Maestro.pasteur.fr
```
module add apptainer
apptainer build docker-miniconda3-jupyter.sif  docker://registry-gitlab.pasteur.fr/tru/docker-miniconda3-jupyter:main
apptainer exec -B /pasteur docker-miniconda3-jupyter.sif jupyter-lab  --no-browser
```

## running from cache only:
```
singularity exec docker://registry-gitlab.pasteur.fr/tru/docker-miniconda3-jupyter:main jupyter-lab  --no-browser
```
